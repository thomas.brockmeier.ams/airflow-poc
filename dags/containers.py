import json
from datetime import timedelta

from airflow import DAG
from airflow.hooks.postgres_hook import PostgresHook
from airflow.operators.http_operator import SimpleHttpOperator
from airflow.operators.python_operator import PythonOperator
from airflow.utils.dates import days_ago

default_args = {
    'owner': 'airflow',
    'depends_on_past': False,
    'start_date': days_ago(2),
    'email': ['airflow@example.com'],
    'email_on_failure': False,
    'email_on_retry': False,
    'retries': 1,
    'retry_delay': timedelta(minutes=5),
}

dag = DAG(
    dag_id='containers',
    default_args=default_args,
    description='Lists all containers in Amsterdam',
    schedule_interval=timedelta(days=1),
)

t1 = SimpleHttpOperator(
    task_id='query_api',
    method='GET',
    http_conn_id='amsterdam_data_point',
    endpoint='afval/v1/containers/',
    dag=dag,
    xcom_push=True,
    retries=3,
    default_args=default_args,
)


def transform_and_save(**kwargs):
    postgres = PostgresHook()
    keys = ['id', 'active', 'operational_date', 'placing_date', 'address']
    ti = kwargs['ti']

    query_result = json.loads(ti.xcom_pull(key=None, task_ids='query_api'))
    results = query_result['results']
    rows = ((x[key] if key is not 'active' else bool(x[key]) for key in keys) for x in results)
    postgres.insert_rows('containers', rows, ['remote_id'] + keys[1:])


t2 = PythonOperator(
    task_id='transform_and_save',
    python_callable=transform_and_save,
    provide_context=True,
    default_args=default_args,
)

t1 >> t2
