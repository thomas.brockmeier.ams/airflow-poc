from datetime import timedelta

from airflow import DAG
from airflow.operators.postgres_operator import PostgresOperator
from airflow.utils.dates import days_ago

default_args = {
    'owner': 'airflow',
    'depends_on_past': False,
    'start_date': days_ago(2),
    'email': ['airflow@example.com'],
    'email_on_failure': False,
    'email_on_retry': False,
    'retries': 1,
    'retry_delay': timedelta(minutes=5),
}

sql = """
CREATE SCHEMA IF NOT EXISTS public;
CREATE TABLE IF NOT EXISTS containers (
    id SERIAL NOT NULL primary key,
    remote_id INT NOT NULL,
    active BOOLEAN NOT NULL,
    operational_date TIMESTAMP NOT NULL,
    placing_date TIMESTAMP NOT NULL,
    address VARCHAR NOT NULL,
    created_on TIMESTAMP DEFAULT NOW() NOT NULL
);
"""

dag = DAG(
    dag_id='initialize_db',
    description='Runs all commands to initialize the database',
    schedule_interval='@once',
    default_args=default_args,
)

t1 = PostgresOperator(
    task_id='create_tables',
    sql=sql,
    dag=dag,
    default_args=default_args,
)
