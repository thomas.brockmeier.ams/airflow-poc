# Airflow PoC
Simple Airflow proof of concept project to pull data from an API and save it to a database. Based on the [Bitnami Airflow setup](https://github.com/bitnami/bitnami-docker-airflow).

### Install Prerequisites
Install [docker](https://www.digitalocean.com/community/tutorials/how-to-install-and-use-docker-on-ubuntu-20-04) and [docker-compose](https://www.digitalocean.com/community/tutorials/how-to-install-and-use-docker-compose-on-ubuntu-20-04)

### Configure Environment
Generate a Fernet key:

```
$ pip install cryptography
$ python -c "from cryptography.fernet import Fernet; print(Fernet.generate_key().decode())"
eGwdxq_NuDn7sBKZD0mIJZafxqabH4hcY319OxnZpoo=
```

Set the `AIRFLOW_FERNET_KEY` environment variable under `airflow`, `airflow-scheduler`, and `airflow-worker` to the generated key.

N.B., actual production keys should not go into version control, and the key above should not be reused for any actual Airflow cluster (or anything else) that lives on the open Internet.

(Optionally) update default settings, e.g., add `AIRFLOW_LOAD_EXAMPLES=no` setting to `airflow`, `airflow-scheduler`, and `airflow-worker` images their environment to disable the example DAGs.

### Spin Up Cluster
```
$ docker-compose up -d
```

This can take a while, around five minutes or so. The Airflow UI will be available at http://127.0.0.1:8080 once done. Log in with username `user` and password `bitnami`.

### Configure Connections
Go to the Admin > Connections tab to view the available connections. Add a new connection to access the Datapunt API:

```
Conn Id: amsterdam_data_point
Conn Type: HTTP
Host: https://api.data.amsterdam.nl
```

Update the `postgres_default` connection with the correct credentials:

```
Host: postgresql
Schema: bitnami_airflow
Login: bn_airflow
Password: bitnami1
```

### Usage
You should now be able to run DAGs to create a containers table, and fill it with some data from the containers API
. The data is saved in the Airflow PostgreSQL instance for demonstration purposes. You should save your production
 data in a dedicated database, and leave the Airflow database alone. Access your data by jumping to a `psql` shell:

```
$ docker-compose exec postgresql /bin/bash
$ psql -U bn_airflow -d bitnami_airflow

# Password: bitnami1

bitnami_airflow=> select * from containers;
```

### Known Limitations
- No security best practices have been followed. Don't put sensitive information in version control
- Data should be saved to another database, i.e., not Airflow's own DB
- Database management should be performed properly
- API pagination is ignored
- ...

### Further Reading
- https://hub.docker.com/r/bitnami/airflow/
- https://github.com/bitnami/bitnami-docker-airflow
